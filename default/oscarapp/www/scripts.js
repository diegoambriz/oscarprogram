document.getElementById("Button").addEventListener("click", myFunction);

function myFunction(){
    var alt1 = document.getElementById("Altitud1").value;
    var chequeo = parseInt(alt1);
    if (chequeo < 0) {
        document.getElementById("Mostrar Densidad").innerHTML = "Error! Altitud Negativa";
        document.getElementById("Mostrar Viscosidad").innerHTML = "Error! Altitud Negativa";
        document.getElementById("Mostrar Densidad").innerHTML = "Error! Altitud Negativa";
        document.getElementById("Mostrar Presion").innerHTML = "Error! Altitud Negativa";
        document.getElementById("Mostrar Temperatura").innerHTML = "Error! Altitud Negativa";
    };
    console.log("------")
    console.log("valor de parse int alt1 es  "+ chequeo)
    var Rd = 287 ;// J/(kg·K)
    console.log('asd');
    //Calculos Temperatura
    var a = -6.5e-3;
    var b = 3e-3;
    var c = -4.5e-3;
    var d = 4e-3;
    var t1 = 288.16;
    var t2 = 216.66;
    var t3 = 282.66;
    var t4 = 165.66;
    if(parseInt(alt1) <= 11000 && parseInt(alt1) >= 0){
        var resultadoTemperatura = t1 - (a*(parseInt(alt1)));
    }
    else if(parseInt(alt1) > 11000 && parseInt(alt1) <= 25000){//Listo
        var resultadoTemperatura = t2 ;
    }
    else if(parseInt(alt1) > 25000 && parseInt(alt1) <= 47000){
        var resultadoTemperatura = t2 +(b*(parseInt(alt1)))  ;
    }
    else if(parseInt(alt1) > 47000 && parseInt(alt1) <= 53000){//Listo
        var resultadoTemperatura = t3 ;
    }
    else if(parseInt(alt1) > 53000 && parseInt(alt1) <= 79000){
        var resultadoTemperatura = t3 - (c*(parseInt(alt1))) ;
    }
    else if(parseInt(alt1) > 79000 && parseInt(alt1) <= 90000){//Listo
        var resultadoTemperatura = t4 ;
    }
    else if(parseInt(alt1) > 90000 && parseInt(alt1) <= 105000){
        var resultadoTemperatura = t4 +(d*(parseInt(alt1))) ;
    }
    else if(parseInt(alt1) > 105000){
        var resultadoTemperatura = 'Ya no hay grafica aqui profe(mayor a 105 km)';
    }
   //Calculos Velocidad
   /*
    if(parseInt(alt1) <= 11000 ){
        var resultadoVelocidad = parseInt(alt1) +'  m/s';
    }
    else if(parseInt(alt1) > 11000 && parseInt(alt1) <= 25000){
        var resultadoVelocidad = 216.66 + '  m/s';
    }
    else if(parseInt(alt1) > 25000 && parseInt(alt1) <= 47000){
        var resultadoVelocidad = parseInt(alt1) +'  m/s' ;
    }
    else if(parseInt(alt1) > 47000 && parseInt(alt1) <= 53000){
        var resultadoVelocidad = 282.66 +'  m/s';
    }
    else if(parseInt(alt1) > 53000 && parseInt(alt1) <= 79000){
        var resultadoVelocidad = parseInt(alt1) +'  m/s';
    }
    else if(parseInt(alt1) > 79000 && parseInt(alt1) <= 90000){
        var resultadoVelocidad = parseInt(alt1) +'  m/s';
    }
    else if(parseInt(alt1) > 90000 && parseInt(alt1) <= 105000){
        var resultadoVelocidad = parseInt(alt1) +' m/s';
    }
    else if(parseInt(alt1) > 105000){
        var resultadoVelocidad = 'Ya no hay grafica aqui profe';
    }*/
    var resultadoVelocidad = Math.sqrt(1.4 * Rd * resultadoTemperatura) + '  m/s' ;


    //Calculos Viscosidad
    console.log('calculos viscosidad  ');
    var c1 = 1.45e-6;
    var c2 = 110.4;
    var visPre = c1 * resultadoTemperatura;
    console.log('vispre  ' + visPre);
    var visPrePotenciado = Math.pow(visPre,1.5);
    console.log('visprePotenciado ' + visPrePotenciado);
    var visSub = resultadoTemperatura + c2;
    console.log('visSub  ' + visSub);
    var resultadoViscosidad = visPrePotenciado / visSub + ' KG / s * m';
    console.log('ressVIscosidad   ' + resultadoViscosidad);


    //Calculos Presion
    if(parseInt(alt1) <= 11000 && parseInt(alt1) >= 0){
        presionProcess = ((parseInt(resultadoTemperatura))/t1); //listo
    }
    else if(parseInt(alt1) > 11000 && parseInt(alt1) <= 25000){ //ISO
        presionProcess = ((parseInt(resultadoTemperatura))/288.16);
    }
    else if(parseInt(alt1) > 25000 && parseInt(alt1) <= 47000){ //listo
        presionProcess = ((parseInt(resultadoTemperatura))/t2);
    }
    else if(parseInt(alt1) > 47000 && parseInt(alt1) <= 53000){ //ISO
        presionProcess = ((parseInt(resultadoTemperatura))/288.16);
    }
    else if(parseInt(alt1) > 53000 && parseInt(alt1) <= 79000){
        presionProcess = ((parseInt(resultadoTemperatura))/t3);
    }
    else if(parseInt(alt1) > 79000 && parseInt(alt1) <= 90000){//ISO
        presionProcess = ((parseInt(resultadoTemperatura))/288.16);
    }
    else if(parseInt(alt1) > 90000 && parseInt(alt1) <= 105000){
        presionProcess = ((parseInt(resultadoTemperatura))/t4);
    }
    
    ///old press
    var presionProcess = ((parseInt(resultadoTemperatura))/288.16);
    console.log('pressionPro  ' + presionProcess);
    // var resultadoPresion = 0 +((presionProcess)*(9.81/a*Rd)*(1.01e5)) ;
    var subpress = parseInt(resultadoTemperatura)*Rd;
    console.log('pressionsub  ' + subpress);
    var presionProcess2 = (9.81/(subpress));
    console.log('pressionPro2  ' + presionProcess2);
    var presionProcess3 = Math.pow(presionProcess, presionProcess2);
    console.log('pressionPro3  ' + presionProcess3)
    var pInt = parseInt(presionProcess3) ;                                                        //RECORDATORIO : NO USAR parseInt con
                                                                                                  //Numeros pequeños
    console.log('pint  ' + pInt)
    if(parseInt(alt1) < 105000 && parseInt(alt1) >= 0){
        var resultadoPresion = presionProcess3 * (101000) ;
    }
    else if(parseInt(alt1) > 105000){
        var resultadoPresion = 'Ya no hay grafica aqui profe(mayor a 105 km)';
    }
    
    console.log('res press  ' + resultadoPresion)



    //Calculos Densidad
    var resultadoDensidad = (parseInt(resultadoPresion) / (Rd * parseInt(resultadoTemperatura)) ) + ' kg/m3';

    //Calculos Presion
    
    //287.058 J/(kg·K)
    
    ////////////////////////////////
    
    
    
    
    
    document.getElementById("Mostrar Velocidad").innerHTML = resultadoVelocidad;
    document.getElementById("Mostrar Viscosidad").innerHTML = resultadoViscosidad;
    document.getElementById("Mostrar Densidad").innerHTML = resultadoDensidad;
    document.getElementById("Mostrar Presion").innerHTML = resultadoPresion + ' pascales';
    document.getElementById("Mostrar Temperatura").innerHTML = resultadoTemperatura +' grados kelvin' ;
    if (chequeo < 0) {
        document.getElementById("Mostrar Densidad").innerHTML = "Error! Altitud Negativa";
        document.getElementById("Mostrar Viscosidad").innerHTML = "Error! Altitud Negativa";
        document.getElementById("Mostrar Densidad").innerHTML = "Error! Altitud Negativa";
        document.getElementById("Mostrar Presion").innerHTML = "Error! Altitud Negativa";
        document.getElementById("Mostrar Temperatura").innerHTML = "Error! Altitud Negativa";
        document.getElementById("Mostrar Velocidad").innerHTML = "Error! Altitud Negativa";
    };
};
   